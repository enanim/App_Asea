/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/ 

var storage = window.localStorage;
var session = window.sessionStorage;

var webview;

var app = {
	// Application Constructor
	initialize: function() {
		this.bindEvents();
	},
	// Bind Event Listeners
	//
	// Bind any events that are required on startup. Common events are:
	// 'load', 'deviceready', 'offline', and 'online'.
	bindEvents: function() {
		document.addEventListener('deviceready', this.onDeviceReady, false);
	},
	// deviceready Event Handler
	//
	// The scope of 'this' is the event. In order to call the 'receivedEvent'
	// function, we must explicitly call 'app.receivedEvent(...);'
	onDeviceReady: function() {
		app.receivedEvent('deviceready');

		if(checkItemID() == true){
			webview = cordova.InAppBrowser.open('http://www.aseaent.com/ci/index.php?id='+session.getItem('itemID'), '_self', 'location=no,clearcache=yes,clearsessioncache=yes');			
			webview.addEventListener('loadstop', function() {
				webview.executeScript({ code: "function qrscan(){ window.location.href='/ci/index.php/mobile/qrscan'; }"});
			});
			
			webview.addEventListener('loadstart', function(event) {
				if(event.url == 'http://www.aseaent.com/ci/index.php/mobile/qrscan'){					
					webview.close();
					session.removeItem('itemID');
					window.location.reload();
					return false;
				}
			});
			document.addEventListener("resume", onResume, false);
		}
	},
	// Update DOM on a Received Event
	receivedEvent: function(id) {
		var parentElement = document.getElementById(id);
		var listeningElement = parentElement.querySelector('.listening');
		var receivedElement = parentElement.querySelector('.received');

		listeningElement.setAttribute('style', 'display:none;');
		receivedElement.setAttribute('style', 'display:block;');

		console.log('Received Event: ' + id);
	}
};


app.initialize();

function onResume()
{
    // Handle the resume event
    alert('resume');
}

window.echo = function(str, callback)
{
    cordova.exec(callback, function(err) {
        callback('Nothing to echo.');
    }, "Echo", "echo", [str]);
};

function checkItemID()
{
	if(!session.getItem('itemID')){
		cordova.plugins.barcodeScanner.scan(
			function (result) {
				if(result.text.split("?id=")[0] == "http://www.aseaent.com/ci/index.php"){
					itemID = Number(result.text.split("?id=")[1]);
					session.setItem('itemID',itemID);
					window.location.reload();
				}else{
					//navigator.app.exitApp();
				}
			}, 
			function (error) {				
				//alert("Scanning failed: " + error);
				alert("바코드 스캔 실패 다시 시도해 주세요");
				checkItemID();
			}
		);
		return false;
	}else{
		return true;
	}
}

function checkLogin()
{
	if(!session.getItem('userID')){
		return false;
	}else{
		return true;
	}
}